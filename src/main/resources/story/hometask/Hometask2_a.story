Scenario: Login

Given I am on the main application page

When I click on element located `By.xpath(//*[text()='Log In'])`
And I enter `vividus.testing@gmail.com` in field located `By.xpath(//*[@id="user"])`
And I wait until element located `By.xpath(//*[@value='Log in with Atlassian'])` appears

When I click on all elements located `By.xpath(//*[@value='Log in with Atlassian'])`
And I click on all elements located `By.xpath(//*[@id="login-submit"]//*[text()='Continue'])`
And I enter `Trololo1` in field located `By.xpath(//*[@id="password"])`
When I click on element located `By.xpath(//*[@id="login-submit"]//*[text()='Log in'])`
Then the page with the URL 'https://trello.com/vividustesting/boards' is loaded
