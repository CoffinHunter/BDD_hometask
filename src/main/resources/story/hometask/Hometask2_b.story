Scenario: login
Given I am on a page with the URL 'https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&hl=en&flowName=GlifWebSignIn&flowEntry=ServiceLogin'


When I enter `vividus.testing@gmail.com` in field located `By.xpath(//*[@id="identifierId"])`
And I click on element located `By.xpath(//*[@id="identifierNext"]//*[text()='Next'])`
And I enter `Trololo1` in field located `By.xpath(//*[@type='password'])`
And I click on element located `By.xpath(//*[text()='Next'])`
Then the page with the URL containing 'https://mail.google.com' is loaded

When I click on element located `By.xpath(//*[text()='Trello'])`
When I click on element located `By.xpath(//*[text()='Verify your email'])`
When I wait until element located `By.xpath(//*[@class='sc-eHgmQL fdgziR'])` appears

//IDK how to switch tab to complete the scenario to verify email